package uehara.app.github.gui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uehara.app.github.R;
import uehara.app.github.api.GithubApi;
import uehara.app.github.dominio.PullRequest;
import uehara.app.github.dominio.Repository;
import uehara.app.github.dominio.RepositoryResponse;
import uehara.app.github.gui.adapters.PullRequestAdapter;
import uehara.app.github.gui.adapters.RepositoryAdapter;
import uehara.app.github.gui.listeners.EndlessRecyclerViewScrollListener;

public class PullRequestActivity extends AppCompatActivity {

    private RecyclerView rvItems;
    private ArrayList<PullRequest> allPullRequests = new ArrayList<PullRequest>();
    private PullRequestAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration mDividerItemDecoration;
    private Toolbar toolbar;
    private TextView status;
    private String repositoryName,repositoryOwnerName;
    private static final String STATE_FILTER = "all";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);

        Intent intent = getIntent();
        repositoryOwnerName = intent.getStringExtra("repositoryOwnerName");
        repositoryName = intent.getStringExtra("repositoryName");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(repositoryName);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        this.getPullRequests();

        status = (TextView) findViewById(R.id.status);

        rvItems = (RecyclerView) findViewById(R.id.rvPullRequest);

        adapter = new PullRequestAdapter(allPullRequests, this);

        rvItems.setAdapter(adapter);

        linearLayoutManager = new LinearLayoutManager(this);

        rvItems.setLayoutManager(linearLayoutManager);

        mDividerItemDecoration = new DividerItemDecoration(
                rvItems.getContext(),
                linearLayoutManager.getOrientation()
        );

        rvItems.addItemDecoration(mDividerItemDecoration);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void getPullRequests(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.URL_BASE))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GithubApi service = retrofit.create(GithubApi.class);

        final Call<List<PullRequest>> callRepositories = service.getPullRequests( repositoryOwnerName,repositoryName ,STATE_FILTER);

        callRepositories.enqueue(new Callback<List<PullRequest>>() {
            @Override
            public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                if (response.code() == 200){

                    allPullRequests = (ArrayList<PullRequest>) response.body();

                    updateStatus();

                    adapter.setPullRequests(allPullRequests);

                    adapter.notifyDataSetChanged();

                }else {
                    Log.d("FALHA NA REQUISIÇÃO ", "CÓDIGO:"+response.code());
                    Log.d(" URL ", call.request().url().toString());
                }
            }

            @Override
            public void onFailure(Call<List<PullRequest>> call, Throwable t) {
                Log.d("FALHA NA REQUISIÇÃO ", "SEM CONEXAO COM A INTERNET");
            }
        });
    }

    public void updateStatus(){
        int opened = 0;
        int closed = 0;

        for ( PullRequest i : allPullRequests){

            //Log.d("STATUS", i.getState());
            if( i.getState().equals("closed") ){
                closed ++;
            }else{
                opened ++;
            }
        }

        String localStatus = opened+" Opened / "+closed+" Closed";

        //Log.d("STATUS FINAL", localStatus);

        this.status.setText(localStatus);

    }

}
