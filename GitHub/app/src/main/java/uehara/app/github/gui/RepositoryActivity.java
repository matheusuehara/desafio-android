package uehara.app.github.gui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uehara.app.github.R;
import uehara.app.github.api.GithubApi;
import uehara.app.github.dominio.Repository;
import uehara.app.github.dominio.RepositoryResponse;
import uehara.app.github.gui.adapters.RepositoryAdapter;
import uehara.app.github.gui.listeners.EndlessRecyclerViewScrollListener;

public class RepositoryActivity extends AppCompatActivity {

    private RecyclerView rvItems;
    private EndlessRecyclerViewScrollListener scrollListener;
    private ArrayList<Repository> allRepositories = new ArrayList<Repository>();
    private RepositoryAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration mDividerItemDecoration;
    private Toolbar toolbar;
    private int pagina = 1;
    private static final String language = "language:Java";
    private static final String sort = "stars";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repository);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        rvItems = (RecyclerView) findViewById(R.id.rvRepositories);

        this.getRepositories(pagina);

        adapter = new RepositoryAdapter(allRepositories, this);

        rvItems.setAdapter(adapter);

        linearLayoutManager = new LinearLayoutManager(this);

        rvItems.setLayoutManager(linearLayoutManager);

        mDividerItemDecoration = new DividerItemDecoration(
                rvItems.getContext(),
                linearLayoutManager.getOrientation()
        );
        rvItems.addItemDecoration(mDividerItemDecoration);

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                pagina ++;
                getRepositories(pagina);

            }
        };

        rvItems.addOnScrollListener(scrollListener);

    }

    public void getRepositories(int pagina){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.URL_BASE))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GithubApi service = retrofit.create(GithubApi.class);

        final Call<RepositoryResponse> callRepositories = service.getRepositories(language,sort,pagina);

        callRepositories.enqueue(new Callback<RepositoryResponse>() {
            @Override
            public void onResponse(Call<RepositoryResponse> call, Response<RepositoryResponse> response) {
                if (response.code() == 200){

                    RepositoryResponse repositoryResponse = response.body();

                    ArrayList<Repository> moreRepositories = repositoryResponse.getItems();

                    int curSize = adapter.getItemCount();

                    allRepositories.addAll(moreRepositories);

                    adapter.notifyItemRangeInserted(curSize, allRepositories.size() - 1);

                }else {
                    Log.d("FALHA NA REQUISIÇÃO ", "CÓDIGO:"+response.code());
                    Log.d(" URL ", call.request().url().toString());
                }
            }

            @Override
            public void onFailure(Call<RepositoryResponse> call, Throwable t) {
                Log.d("FALHA NA REQUISIÇÃO ", "SEM CONEXAO COM A INTERNET");
            }
        });
    }

}
