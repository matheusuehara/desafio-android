package uehara.app.github.api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import uehara.app.github.dominio.PullRequest;
import uehara.app.github.dominio.RepositoryResponse;

public interface GithubApi {

    @GET("search/repositories")
    Call<RepositoryResponse> getRepositories(@Query("q") String language,
                                             @Query("sort") String sort,
                                             @Query("page") int pagina);

    //TODO ajeitar interface
    @GET("repos/{owner}/{repository}/pulls")
    Call<List<PullRequest>> getPullRequests(@Path("owner") String owner,
                                            @Path("repository") String repository,
                                            @Query("state") String state);

}
