package uehara.app.github.gui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;

import java.util.ArrayList;
import uehara.app.github.R;
import uehara.app.github.dominio.Repository;
import uehara.app.github.dominio.User;
import uehara.app.github.gui.PullRequestActivity;
import uehara.app.github.util.CircularNetworkImageView;
import uehara.app.github.util.CustomVolleyRequest;

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.MyViewHolder> {


    private ArrayList<Repository> repositories;
    public Context context ;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView repository_name, repository_description, repository_star,repository_fork, repository_username, repository_full_name;
        public ImageLoader imageLoader;

        public CircularNetworkImageView repository_user_image;

        public MyViewHolder(final View view) {
            super(view);

            view.setOnClickListener(this);

            repository_user_image = (CircularNetworkImageView) view.findViewById(R.id.repository_user_image);
            repository_name = (TextView) view.findViewById(R.id.repository_name);
            repository_description = (TextView) view.findViewById(R.id.repository_description);
            repository_star = (TextView) view.findViewById(R.id.star_value);
            repository_fork = (TextView) view.findViewById(R.id.fork_value);
            repository_username = (TextView) view.findViewById(R.id.repository_username);
            repository_full_name = (TextView) view.findViewById(R.id.repository_full_name);

        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            //Log.d("DEBUG DO ADAPTER", "Elemento " + getAdapterPosition() + " clicado.");
            //Log.d("DEBUG DO ADAPTER", "Elemento :" + repositories.get(getAdapterPosition()).getName() + " clicado.");

            try {
                Repository repository = repositories.get(position);
                Intent i = new Intent(context,PullRequestActivity.class);

                i.putExtra("repositoryName", repository.getName());
                i.putExtra("repositoryOwnerName", repository.getOwner().getLogin());

                context.startActivity(i);
            }catch (ArrayIndexOutOfBoundsException e){
                e.printStackTrace();
            }


        }
    }

    public RepositoryAdapter(ArrayList<Repository> repositories, Context contexto) {
        this.context = contexto;
        this.repositories = repositories;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.lv_item_repository, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        Repository repository = repositories.get(position);
        User owner = repository.getOwner();

        holder.repository_name.setText(repository.getName());
        holder.repository_description.setText(repository.getDescription());
        holder.repository_star.setText( String.valueOf(repository.getStargazers_count()) );
        holder.repository_fork.setText( String.valueOf(repository.getForks_count()) );
        holder.repository_username.setText(owner.getLogin());

        //Não foi encontrado o atributo correspondente na api
        holder.repository_full_name.setText("");

        String imageProfile;
        if (owner.getAvatar_url() != null ) {
            imageProfile = owner.getAvatar_url();
        }else{
            imageProfile = "";
        }
        holder.imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
        holder.imageLoader.get(imageProfile,ImageLoader.getImageListener(holder.repository_user_image,
                R.mipmap.ic_launcher,
                R.mipmap.ic_launcher));
        holder.repository_user_image.setImageUrl(imageProfile, holder.imageLoader);
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }
}