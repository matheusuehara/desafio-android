package uehara.app.github.gui.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import java.util.ArrayList;

import uehara.app.github.R;
import uehara.app.github.dominio.PullRequest;
import uehara.app.github.dominio.User;
import uehara.app.github.util.CircularNetworkImageView;
import uehara.app.github.util.CustomVolleyRequest;

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.MyViewHolder> {

    private ArrayList<PullRequest> pullRequests;
    public Context context ;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView pull_request_title, pull_request_body, pull_request_username, pull_request_full_name;
        public ImageLoader imageLoader;
        public CircularNetworkImageView pull_request_user_image;


        public MyViewHolder(final View view) {
            super(view);

            view.setOnClickListener(this);

            pull_request_user_image = (CircularNetworkImageView) view.findViewById(R.id.pull_request_user_image);
            pull_request_title = (TextView) view.findViewById(R.id.pull_request_title);
            pull_request_body = (TextView) view.findViewById(R.id.pull_request_body);
            pull_request_username = (TextView) view.findViewById(R.id.pull_request_username);
            pull_request_full_name = (TextView) view.findViewById(R.id.pull_request_full_name);

        }

        @Override
        public void onClick(View v) {
            Log.d("DEBUG DO ADAPTER", "Elemento " + getAdapterPosition() + " clicado.");
            Log.d("DEBUG DO ADAPTER", "Elemento :" + pullRequests.get(getAdapterPosition()).getTitle() + " clicado.");

            int position = getAdapterPosition();
            String url = pullRequests.get(position).getHtml_url();

            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            context.startActivity(i);
        }
    }

    public PullRequestAdapter(ArrayList<PullRequest> pullRequests, Context contexto) {
        this.context = contexto;
        this.pullRequests = pullRequests;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.lv_item_pull_request, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        PullRequest pullRequest = pullRequests.get(position);

        User user = pullRequest.getUser();

        holder.pull_request_title.setText(pullRequest.getTitle() + "");
        holder.pull_request_body.setText(pullRequest.getBody()+ "");
        holder.pull_request_username.setText(user.getLogin()+ "");

        //Não foi encontrado o atributo correspondente na api
        holder.pull_request_full_name.setText( "" );

        String imageProfile;
        if (user.getAvatar_url() != null ) {
            imageProfile = user.getAvatar_url();
        }else{
            imageProfile = "";
        }
        holder.imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
        holder.imageLoader.get(imageProfile,ImageLoader.getImageListener(holder.pull_request_user_image,
                R.mipmap.ic_launcher,
                R.mipmap.ic_launcher));
        holder.pull_request_user_image.setImageUrl(imageProfile, holder.imageLoader);
    }

    @Override
    public int getItemCount() {
        return pullRequests.size();
    }

    public ArrayList<PullRequest> getPullRequests() {
        return pullRequests;
    }

    public void setPullRequests(ArrayList<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;
    }
}