package uehara.app.github.dominio;

import java.util.ArrayList;

/**
 * Created by uehara on 19/12/17.
 */

public class RepositoryResponse {

    private int total_count;
    private boolean incomplete_results;
    private ArrayList<Repository> items;

    public int getTotal_count() {
        return total_count;
    }

    public void setTotal_count(int total_count) {
        this.total_count = total_count;
    }

    public boolean isIncomplete_results() {
        return incomplete_results;
    }

    public void setIncomplete_results(boolean incomplete_results) {
        this.incomplete_results = incomplete_results;
    }

    public ArrayList<Repository> getItems() {
        return items;
    }

    public void setItems(ArrayList<Repository> items) {
        this.items = items;
    }
}
